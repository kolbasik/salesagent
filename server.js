var express = require('express'),
    path = require('path');

var app = express();

app.configure(function() {
    app.use(express.static(path.join(__dirname, 'client')));
    app.use(express.favicon());
    app.use(express.logger('dev'));
    app.use(express.json());
    app.use(express.urlencoded());
    app.use(express.bodyParser());
    app.use(express.cookieParser('nebula'));
    app.use(express.methodOverride());
    app.use(app.router);
});

app.configure('development', function() {
    app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.listen(process.env.PORT);