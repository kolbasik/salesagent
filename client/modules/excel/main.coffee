'use strict'

define 'excel', ["app", "css!excel/style.css"], (app) ->

	app.controller 'excel', ($scope, $parse) ->
		$scope.columns = ['A', 'B', 'C', 'D']
		$scope.rows = [1, 2, 3, 4]
		$scope.cells = {}
		$scope.Math = Math
	
		pattern = /[A-Z]\d+/g;
		process = (exp) -> 
			exp.replace pattern, (ref) ->
				'compute("' + ref + '")'

		$scope.compute = (cell) ->
			try
				result = $parse(process($scope.cells[cell]))($scope)
			catch
				result = 0
			result
		
		return

	return this;
