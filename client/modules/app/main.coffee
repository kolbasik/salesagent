﻿'use strict'

define 'app', ['jquery', 'bootstrap', 'angular'], ($, bootstrap, angular) ->
	app = angular.module('app', ['ngRoute'])
	app.start = -> 
		$ -> 
			angular.bootstrap(document, ['app'])
			return
		return
	return app