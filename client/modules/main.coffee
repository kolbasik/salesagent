﻿'use strict'

require.config({
	baseUrl: '/modules',
	paths: {
		'jquery': 'http://code.jquery.com/jquery.min',
		'bootstrap': 'http://getbootstrap.com/dist/js/bootstrap.min',
		'angular-core': 'https://ajax.googleapis.com/ajax/libs/angularjs/1.2.12/angular.min',
		'angular-route': 'https://ajax.googleapis.com/ajax/libs/angularjs/1.2.12/angular-route.min'
	},
	shim: {
		'bootstrap': {
			deps: [
				'jquery',
				'css!http://getbootstrap.com/dist/css/bootstrap.min.css',
				'css!http://getbootstrap.com/dist/css/bootstrap-theme.min.css'
			]
		},
		'angular-core': {
			exports: 'angular'
		},
		'angular-route': {
			deps: ['angular-core']
		}
	},
	packages: [ 'app' ]
});

resource = require.resource = (name) ->
	if /^(https?:)?\/\//.test(name)
		url = name
	else
		baseUrl = require.s.contexts._.config.baseUrl;
		url = baseUrl + name
	return url

define 'css', {
	load: (name, require, load, config) ->
		inject = (filename) ->
			head = document.getElementsByTagName('head')[0];
			link = document.createElement('link');
			link.href = filename;
			link.rel = 'stylesheet';
			link.type = 'text/css';
			head.appendChild(link);
		inject(resource(name));
		load(true);
}

define 'angular', ['angular-core', 'angular-route'], () ->
	return angular